import axios from "axios";

const instance = axios.create({
  baseURL: 'http://s302task.kenzietandun.com/api',
  timeout: 5000
});

export default {
  retrieveAllTeams: () => instance.get(`/teams/`),

  retrieveTeamSprints: teamId => instance.get(`/teams/${teamId}/sprints`),

  retrieveSprintStories: (teamId, sprintId) =>
    instance.get(`/teams/${teamId}/sprints/${sprintId}/stories`)
};

const routes = [
  {
    name: "teams",
    path: "/",
    component: () => import("layouts/Teams.vue")
  },
  {
    name: "team",
    path: "/teams/:teamId/sprints",
    props: true,
    component: () => import("layouts/SingleTeam.vue")
  },
  {
    name: "story",
    path: "/teams/:teamId/sprints/:sprintId/stories",
    props: true,
    component: () => import("layouts/SingleStory.vue")
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "*",
    component: () => import("pages/Error404.vue")
  }
];

export default routes;

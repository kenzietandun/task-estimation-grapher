package com.kenzietandun.taskestimationgrapher.config;

import com.kenzietandun.taskestimationgrapher.commutils.StatefulRestTemplateInterceptor;
import org.springframework.boot.web.client.ClientHttpRequestFactorySupplier;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder templateBuilder) {
        return templateBuilder
                .requestFactory(new ClientHttpRequestFactorySupplier())
                .interceptors(new StatefulRestTemplateInterceptor())
                .build();
    }
}

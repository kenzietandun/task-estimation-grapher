package com.kenzietandun.taskestimationgrapher.controller;

import com.kenzietandun.taskestimationgrapher.dto.response.SingleSprintResponse;
import com.kenzietandun.taskestimationgrapher.dto.response.SingleTaskResponse;
import com.kenzietandun.taskestimationgrapher.dto.response.SingleTeamResponse;
import com.kenzietandun.taskestimationgrapher.model.Sprint;
import com.kenzietandun.taskestimationgrapher.model.Task;
import com.kenzietandun.taskestimationgrapher.model.Team;
import com.kenzietandun.taskestimationgrapher.service.SprintService;
import com.kenzietandun.taskestimationgrapher.service.StoryService;
import com.kenzietandun.taskestimationgrapher.service.TaskService;
import com.kenzietandun.taskestimationgrapher.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class MainController {

    @Autowired
    TeamService teamService;

    @Autowired
    StoryService storyService;

    @Autowired
    TaskService taskService;

    @Autowired
    SprintService sprintService;

    @GetMapping("/teams")
    public List<SingleTeamResponse> retrieveAllTeams() {
        return teamService.readAll();
    }

    @GetMapping("/teams/{teamId}")
    public SingleTeamResponse retrieveSingleTeamById(@PathVariable("teamId") Long teamId) {
        Team team = teamService.readOne(teamId);
        return SingleTeamResponse.of(team);
    }

    @GetMapping("/teams/{teamId}/sprints")
    public List<SingleSprintResponse> retrieveSprintsByTeamById(@PathVariable("teamId") Long teamId) {
        List<Sprint> sprints = sprintService.readAllByTeamId(teamId);
        return sprints.stream().map(SingleSprintResponse::of).collect(Collectors.toList());
    }

    @GetMapping("/teams/{teamId}/sprints/{sprintId}/stories")
    public List<SingleTaskResponse> retrieveStoryBySprintById(@PathVariable("teamId") Long teamId,
                                                              @PathVariable("sprintId") Long sprintId)
    {
        List<Task> tasks = taskService.readBySprintId(teamId, sprintId);
        return tasks.stream().map(SingleTaskResponse::of).collect(Collectors.toList());
    }

}

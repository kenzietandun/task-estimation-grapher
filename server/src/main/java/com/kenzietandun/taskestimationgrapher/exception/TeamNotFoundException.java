package com.kenzietandun.taskestimationgrapher.exception;

public class TeamNotFoundException extends RuntimeException {

    private static final String MSG = "Team not found: ";
    public TeamNotFoundException(Long teamId) {
        super(MSG + teamId);
    }
}

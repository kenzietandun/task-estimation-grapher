package com.kenzietandun.taskestimationgrapher.exception;

public class SprintNotFoundException extends RuntimeException {

    private static final String MSG = "Sprint not found: ";
    public SprintNotFoundException(Long id) {
        super(MSG + id);
    }
}

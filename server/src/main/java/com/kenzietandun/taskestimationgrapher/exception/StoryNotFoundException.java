package com.kenzietandun.taskestimationgrapher.exception;

public class StoryNotFoundException extends RuntimeException {

    private static final String MSG = "Story not found: ";
    public StoryNotFoundException(Long id) {
        super(MSG + id);
    }
}

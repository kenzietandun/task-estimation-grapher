package com.kenzietandun.taskestimationgrapher.repository;

import com.kenzietandun.taskestimationgrapher.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamRepository extends JpaRepository<Team, Long> {
}

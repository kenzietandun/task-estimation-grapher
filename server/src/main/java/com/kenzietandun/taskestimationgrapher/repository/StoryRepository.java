package com.kenzietandun.taskestimationgrapher.repository;

import com.kenzietandun.taskestimationgrapher.model.Sprint;
import com.kenzietandun.taskestimationgrapher.model.Story;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StoryRepository extends JpaRepository<Story, Long> {
}

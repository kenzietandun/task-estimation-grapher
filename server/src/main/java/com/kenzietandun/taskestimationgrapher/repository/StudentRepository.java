package com.kenzietandun.taskestimationgrapher.repository;

import com.kenzietandun.taskestimationgrapher.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}

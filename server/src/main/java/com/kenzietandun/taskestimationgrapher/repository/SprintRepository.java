package com.kenzietandun.taskestimationgrapher.repository;

import com.kenzietandun.taskestimationgrapher.model.Sprint;
import com.kenzietandun.taskestimationgrapher.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SprintRepository extends JpaRepository<Sprint, Long> {

    List<Sprint> findAllByTeam(Team team);
}

package com.kenzietandun.taskestimationgrapher.repository;

import com.kenzietandun.taskestimationgrapher.model.Sprint;
import com.kenzietandun.taskestimationgrapher.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Long> {

    List<Task> findAllByStory_Sprint(Sprint s);
}

package com.kenzietandun.taskestimationgrapher.service;

import com.kenzietandun.taskestimationgrapher.dto.agilefant.SprintDTO;
import com.kenzietandun.taskestimationgrapher.dto.agilefant.SprintResponse;
import com.kenzietandun.taskestimationgrapher.exception.TeamNotFoundException;
import com.kenzietandun.taskestimationgrapher.model.Sprint;
import com.kenzietandun.taskestimationgrapher.model.Team;
import com.kenzietandun.taskestimationgrapher.repository.SprintRepository;
import com.kenzietandun.taskestimationgrapher.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SprintService {

    @Autowired
    SprintRepository sprintRepository;

    @Autowired
    TeamRepository teamRepository;

    public Sprint create(SprintDTO sprintDTO) {
        Sprint sprint = new Sprint();
        sprint.setId(sprintDTO.getId());
        sprint.setName(sprintDTO.getTitle());

        return sprintRepository.save(sprint);
    }

    public List<Sprint> readAllByTeamId(Long teamId) {
        Team team = teamRepository.findById(teamId).orElseThrow(() -> new TeamNotFoundException(teamId));
        return sprintRepository.findAllByTeam(team);
    }
}

package com.kenzietandun.taskestimationgrapher.service;

import com.kenzietandun.taskestimationgrapher.dto.agilefant.TeamDTO;
import com.kenzietandun.taskestimationgrapher.dto.response.SingleTeamResponse;
import com.kenzietandun.taskestimationgrapher.exception.TeamNotFoundException;
import com.kenzietandun.taskestimationgrapher.model.Team;
import com.kenzietandun.taskestimationgrapher.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeamService {

    @Autowired
    TeamRepository teamRepository;

    public Team create(TeamDTO teamDTO) {
        Team team = new Team();
        team.setId(teamDTO.getId());
        team.setName(teamDTO.getTitle());

        return teamRepository.save(team);
    }

    public List<SingleTeamResponse> readAll() {
        return teamRepository.findAll().stream()
                .map(SingleTeamResponse::of)
                .collect(Collectors.toList());
    }

    public Team readOne(Long teamId) {
        return teamRepository.findById(teamId).orElseThrow(() -> new TeamNotFoundException(teamId));
    }
}

package com.kenzietandun.taskestimationgrapher.service;

import com.kenzietandun.taskestimationgrapher.dto.agilefant.StoryDTO;
import com.kenzietandun.taskestimationgrapher.exception.SprintNotFoundException;
import com.kenzietandun.taskestimationgrapher.exception.StoryNotFoundException;
import com.kenzietandun.taskestimationgrapher.exception.TeamNotFoundException;
import com.kenzietandun.taskestimationgrapher.model.Story;
import com.kenzietandun.taskestimationgrapher.repository.SprintRepository;
import com.kenzietandun.taskestimationgrapher.repository.StoryRepository;
import com.kenzietandun.taskestimationgrapher.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StoryService {

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    SprintRepository sprintRepository;

    @Autowired
    StoryRepository storyRepository;

    public Story create(StoryDTO storyDTO) {
        Story s = new Story();
        s.setId(storyDTO.getId());
        s.setName(storyDTO.getName());

        return storyRepository.save(s);
    }
}

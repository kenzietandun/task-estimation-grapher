package com.kenzietandun.taskestimationgrapher.service;

import com.kenzietandun.taskestimationgrapher.dto.agilefant.TaskDTO;
import com.kenzietandun.taskestimationgrapher.exception.SprintNotFoundException;
import com.kenzietandun.taskestimationgrapher.exception.TeamNotFoundException;
import com.kenzietandun.taskestimationgrapher.model.Sprint;
import com.kenzietandun.taskestimationgrapher.model.Task;
import com.kenzietandun.taskestimationgrapher.repository.SprintRepository;
import com.kenzietandun.taskestimationgrapher.repository.TaskRepository;
import com.kenzietandun.taskestimationgrapher.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskService {
    @Autowired
    TeamRepository teamRepository;

    @Autowired
    SprintRepository sprintRepository;

    @Autowired
    TaskRepository taskRepository;

    public Task create(TaskDTO taskDTO) {
        Task task = new Task();
        task.setId(taskDTO.getId());
        task.setDescription(taskDTO.getName());
        task.setEstimatedTime(taskDTO.getOriginalEstimate());
        task.setActualTime(taskDTO.getEffortSpent());

        return taskRepository.save(task);
    }


    public List<Task> readBySprintId(Long teamId, Long sprintId) {
        teamRepository.findById(teamId).orElseThrow(() -> new TeamNotFoundException(teamId));
        Sprint sprint = sprintRepository.findById(sprintId).orElseThrow(() -> new SprintNotFoundException(sprintId));

        return taskRepository.findAllByStory_Sprint(sprint);
    }


}

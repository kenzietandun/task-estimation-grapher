package com.kenzietandun.taskestimationgrapher.service;

import com.kenzietandun.taskestimationgrapher.dto.agilefant.TaskResponsiblesDTO;
import com.kenzietandun.taskestimationgrapher.model.Student;
import com.kenzietandun.taskestimationgrapher.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

    @Autowired
    StudentRepository studentRepository;

    public Student create(TaskResponsiblesDTO taskResponsiblesDTO) {
        Student student = new Student();
        student.setId(taskResponsiblesDTO.getId());
        student.setUsercode(taskResponsiblesDTO.getInitials());

        return studentRepository.save(student);
    }

}

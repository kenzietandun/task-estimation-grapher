package com.kenzietandun.taskestimationgrapher.service;

import com.kenzietandun.taskestimationgrapher.dto.agilefant.SprintResponse;
import com.kenzietandun.taskestimationgrapher.dto.agilefant.TeamDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.logging.Logger;

@Service
public class AgilefantService {

    private final Logger logger = Logger.getLogger(getClass().getName());

    private final String AGILEFANT_URL = "https://agilefant.csse.canterbury.ac.nz:8443/agilefant302_2020/";

    @Autowired
    Environment environment;

    @Autowired
    RestTemplate restTemplate;

    public void login() {
        logger.info("Logging in to Agilefant");
        String loginUrl = AGILEFANT_URL + "j_spring_security_check";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        String username = environment.getProperty("AGILEFANT_USERNAME");
        String password = environment.getProperty("AGILEFANT_PASSWORD");

        String request = String.format("j_username=%s&j_password=%s", username, password);
        HttpEntity<String> entity = new HttpEntity<>(request, headers);

        logger.info("Sending POST request to login endpoint");
        restTemplate.postForObject(loginUrl, entity, Void.class);
        logger.info("Sent request to login endpoint");
    }

    public List<TeamDTO> retrieveTeamsData() {
        String teamDataURL= AGILEFANT_URL + "ajax/menuData.action";
        ResponseEntity<List<TeamDTO>> response = restTemplate.exchange(
                teamDataURL,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                });

        return response.getBody();
    }

    public SprintResponse retrieveSprintById(Long sprintId) {
        String sprintURL = AGILEFANT_URL + "ajax/iterationData.action?iterationId=" + sprintId;
        return restTemplate.getForObject(sprintURL, SprintResponse.class);
    }

}

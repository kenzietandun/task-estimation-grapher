package com.kenzietandun.taskestimationgrapher.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "task")
public class Task {
    @Id
    private Long id;
    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "task_assignnees",
            joinColumns = @JoinColumn(name = "task_id"),
            inverseJoinColumns = @JoinColumn(name = "student_id"))
    private Set<Student> assignees = new HashSet<>();
    private String description;
    private int estimatedTime;
    private int actualTime;
    @ManyToOne
    @JoinColumn(name = "story_id")
    private Story story;

    public void addAssignees(Student s) {
        assignees.add(s);
    }

    public void removeAssignees(Student s) {
        assignees.remove(s);
    }
}
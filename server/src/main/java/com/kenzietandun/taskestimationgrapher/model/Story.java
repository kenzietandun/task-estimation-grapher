package com.kenzietandun.taskestimationgrapher.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "story")
public class Story {
    @Id
    private Long id;
    private String name;
    @ManyToOne
    @JoinColumn(name = "sprint_id")
    private Sprint sprint;
    @OneToMany(mappedBy = "story", cascade = CascadeType.ALL)
    private Set<Task> tasks = new HashSet<>();

    public void addTask(Task t) {
        tasks.add(t);
    }

    public void removeTask(Task t) {
        tasks.remove(t);
    }

}

package com.kenzietandun.taskestimationgrapher.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "sprint")
public class Sprint {

    @Id
    private Long id;
    private String name;
    @ManyToOne
    @JoinColumn(name = "team_id")
    private Team team;
    @OneToMany(mappedBy = "sprint", cascade = CascadeType.ALL)
    private Set<Story> stories = new HashSet<>();

    public void addStory(Story s) {
        stories.add(s);
    }

    public void removeStory(Story s) {
        stories.remove(s);
    }

}

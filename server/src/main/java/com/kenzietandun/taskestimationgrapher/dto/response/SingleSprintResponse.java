package com.kenzietandun.taskestimationgrapher.dto.response;

import com.kenzietandun.taskestimationgrapher.model.Sprint;
import com.kenzietandun.taskestimationgrapher.model.Story;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class SingleSprintResponse {

    private Long id;
    private String name;
    private List<CompactSingleStoryReponse> stories;

    public static SingleSprintResponse of(Sprint sprint) {
        SingleSprintResponse response = new SingleSprintResponse();
        response.setId(sprint.getId());
        response.setName(sprint.getName());

        List<CompactSingleStoryReponse> stories = sprint.getStories().stream()
                .map(CompactSingleStoryReponse::of)
                .collect(Collectors.toList());
        response.setStories(stories);

        return response;
    }

}

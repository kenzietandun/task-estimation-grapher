package com.kenzietandun.taskestimationgrapher.dto.response;

import com.kenzietandun.taskestimationgrapher.model.Story;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class SingleStoryResponse {

    private Long id;
    private String name;
    private Long sprintId;
    private List<SingleTaskResponse> tasks;

    public static SingleStoryResponse of(Story story) {
        SingleStoryResponse response = new SingleStoryResponse();
        response.setId(story.getId());
        response.setName(story.getName());
        response.setSprintId(story.getSprint().getId());

        List<SingleTaskResponse> tasks = story.getTasks()
                .stream()
                .map(SingleTaskResponse::of)
                .collect(Collectors.toList());
        response.setTasks(tasks);

        return response;
    }

}

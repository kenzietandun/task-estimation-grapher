package com.kenzietandun.taskestimationgrapher.dto.agilefant;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SprintDTO {

    private Long id;
    private String title;
}

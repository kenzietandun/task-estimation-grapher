package com.kenzietandun.taskestimationgrapher.dto.agilefant;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class SprintResponse {

    private String description;
    private List<StoryDTO> rankedStories;

}

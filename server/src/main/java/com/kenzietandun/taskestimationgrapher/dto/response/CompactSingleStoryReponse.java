package com.kenzietandun.taskestimationgrapher.dto.response;

import com.kenzietandun.taskestimationgrapher.model.Story;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CompactSingleStoryReponse {

    private Long id;
    private String name;

    public static CompactSingleStoryReponse of(Story story) {
        CompactSingleStoryReponse response = new CompactSingleStoryReponse();
        response.setId(story.getId());
        response.setName(story.getName());
        return response;
    }
}

package com.kenzietandun.taskestimationgrapher.dto.agilefant;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TaskResponsiblesDTO {

    private Long id;
    private String fullName;
    private String initials;

}

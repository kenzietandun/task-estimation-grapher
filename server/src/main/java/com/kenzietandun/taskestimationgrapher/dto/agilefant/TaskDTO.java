package com.kenzietandun.taskestimationgrapher.dto.agilefant;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TaskDTO {

    private Long id;
    private String name;
    private int originalEstimate;
    private int effortSpent;
    private List<TaskResponsiblesDTO> responsibles;

}
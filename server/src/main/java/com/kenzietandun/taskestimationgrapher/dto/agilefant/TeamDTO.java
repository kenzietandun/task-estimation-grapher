package com.kenzietandun.taskestimationgrapher.dto.agilefant;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TeamDTO {

    private Long id;
    private String title;
    private List<ProductDTO> children;

}

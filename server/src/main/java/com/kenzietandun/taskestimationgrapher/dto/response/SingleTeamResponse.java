package com.kenzietandun.taskestimationgrapher.dto.response;

import com.kenzietandun.taskestimationgrapher.model.Team;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SingleTeamResponse {

    private Long id;
    private String name;


    public static SingleTeamResponse of(Team team) {
        SingleTeamResponse response = new SingleTeamResponse();
        response.setId(team.getId());
        response.setName(team.getName());
        return response;
    }
}

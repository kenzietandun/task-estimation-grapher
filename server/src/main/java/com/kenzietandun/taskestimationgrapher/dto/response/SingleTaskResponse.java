package com.kenzietandun.taskestimationgrapher.dto.response;

import com.kenzietandun.taskestimationgrapher.model.Task;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SingleTaskResponse {

    private Long id;
    private String description;
    private int estimatedTime;
    private int actualTime;

    public static SingleTaskResponse of(Task task) {
        SingleTaskResponse response = new SingleTaskResponse();
        response.setId(task.getId());
        response.setDescription(task.getDescription());
        response.setEstimatedTime(task.getEstimatedTime());
        response.setActualTime(task.getActualTime());
        return response;
    }
}

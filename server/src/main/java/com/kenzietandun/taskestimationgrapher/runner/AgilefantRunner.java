package com.kenzietandun.taskestimationgrapher.runner;

import com.kenzietandun.taskestimationgrapher.dto.agilefant.*;
import com.kenzietandun.taskestimationgrapher.model.*;
import com.kenzietandun.taskestimationgrapher.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.logging.Logger;

@Component
public class AgilefantRunner {

    private final Logger logger = Logger.getLogger(getClass().getName());
    private final int DELAY = 1000 * 60 * 60;

    @Autowired
    AgilefantService agilefantService;

    @Autowired
    TeamService teamService;

    @Autowired
    StoryService storyService;

    @Autowired
    TaskService taskService;

    @Autowired
    StudentService studentService;

    @Autowired
    SprintService sprintService;

    @Scheduled(fixedDelay = DELAY)
    @Transactional
    public void run() {
        agilefantService.login();
        for (TeamDTO eachTeam : agilefantService.retrieveTeamsData()) {
            if (!eachTeam.getTitle().startsWith("Team")) {
                continue;
            }

            Team team = teamService.create(eachTeam);
            logger.info("Created team: " + team.getName());

            for (SprintDTO child : eachTeam.getChildren().get(0).getChildren()) {
                Sprint sprint = sprintService.create(child);
                logger.info("Created sprint: " + sprint.getName());
                team.addSprint(sprint);
                sprint.setTeam(team);

                SprintResponse sprintResponse = agilefantService.retrieveSprintById(child.getId());
                for (StoryDTO story : sprintResponse.getRankedStories()) {
                    Story s = storyService.create(story);
                    logger.info("Created story: " + s.getName());
                    sprint.addStory(s);
                    s.setSprint(sprint);

                    for (TaskDTO task : story.getTasks()) {
                        Task t = taskService.create(task);
                        logger.info("Created task: " + t.getDescription());
                        t.setStory(s);
                        s.addTask(t);

                        for (TaskResponsiblesDTO responsible : task.getResponsibles()) {
                            Student student = studentService.create(responsible);
                            student.addTask(t);
                            t.addAssignees(student);
                        }

                    }

                }
            }
        }
    }

}
